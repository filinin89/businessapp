package service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import userData.Account;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class Transaction {

    private Logger logger = LoggerFactory.getLogger(Transaction.class);
    private ReentrantLock reentrantLock = new ReentrantLock();
    private static volatile AtomicInteger numbOfTransaction= new AtomicInteger(0);


    public void transactionGo(Account acc1, Account acc2, long moneyAmount, int maxTransaction) throws InterruptedException { // можно добавить еще один акк и ереводить с первого счета еще и ему

        if(numbOfTransaction.get() < maxTransaction) {

            reentrantLock.lock(); // теперь только один поток имеет доступ к этой секции, остальные потоки ожидают снятия блокировки

            try {
                if (acc1.getBalance() < moneyAmount) {
                    throw new UnacceptableTransaction("Баланс на счету меньше, чем переводимая сумма"); // можно добавить Succes при успешном выполнении
                }
                if (acc1.equals(acc2)) {
                    throw new UnacceptableTransaction("Перевод по ошибке на один и тот же аккаунт");
                }
                if(acc1 == null || acc2 == null){
                    throw new UnacceptableTransaction("Аккаунта не существует");
                }

                acc1.setBalance(acc1.getBalance() - moneyAmount); // перевод с одного счета на другой
                acc2.setBalance(acc2.getBalance() + moneyAmount);
                numbOfTransaction.incrementAndGet();
                logger.info("Деньги {} переведены со счета  {}  на счет  {}  Номер транзакции {}  Успех", moneyAmount, acc1.getName(), acc2.getName(), numbOfTransaction );

            } catch (Exception e) {
                logger.error(e.getMessage());
            } finally {
                reentrantLock.unlock(); // обязательно разблокировать здесь, так как может произойти ошибка и остальные потоки будут заблокированы
            }
        } else{
            Thread.currentThread().interrupt();
        }


    }
}
