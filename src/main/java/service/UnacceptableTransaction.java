package service;

public class UnacceptableTransaction extends Exception{


    public UnacceptableTransaction(String message){
        super(message);
    }


}
