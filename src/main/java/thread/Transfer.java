package thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.Transaction;
import userData.Account;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class Transfer implements Runnable {

    private Random random = new Random();
    private Transaction transaction = new Transaction(); // может не создавать экземаляр, а использовать статический метод
    private ArrayList<Account> accounts;
    private int maxTransaction;




    public Transfer(ArrayList<Account> accounts, int maxTransaction, Transaction transaction) {
        this.accounts = accounts;
        this.maxTransaction = maxTransaction;
    }


    @Override
    public void run() {
        Account randAcc1, randAcc2;
        int randMoneyAmount;

        while (!Thread.currentThread().isInterrupted()) { //    transaction.getNumbOfTransaction() < maxTransaction  !Thread.currentThread().isInterrupted()
            randAcc1 = accounts.get(random.nextInt(accounts.size()));
            randAcc2 = accounts.get(random.nextInt(accounts.size()));
            randMoneyAmount = random.nextInt(100);
            try {
                transaction.transactionGo(randAcc1, randAcc2, randMoneyAmount, maxTransaction); // рандом можно здесь
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
