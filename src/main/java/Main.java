import utilities.AccountCreator;
import utilities.AccountManager;

public class Main {



    public static void main(String[] args) {

        final int threadCount = 10;
        final int maxTransaction = 10000;


        // 1. создадим аккаунты и запишем их в файлы
        AccountCreator creator = new AccountCreator();
        creator.go();

        // 2. перед Обменом cчитаем с файлов аккаунты и посмотрим баланс и общую суммму
        AccountManager accountManager = new AccountManager();
        accountManager.loadAccounts();
        accountManager.printBalanceAndSum();

        // 3. делаем Обмен
        accountManager.startExchange(threadCount, maxTransaction);

        // 4. после Обмена
        accountManager.printBalanceAndSum();


    }
}
