package utilities;

import userData.Account;

import java.util.Random;

public class AccountCreator {

    private FileProcesses fileProcesses = new FileProcesses();
    private Random random = new Random();
    private String names[] = {"Артем", "Елена", "Максим", "Иван", "Михаил", "Юлия", "Дмитрий", "Андрей", "Татьяна", "Леонид"};


    // путь к файлу мы можем вынести в Main и здесь просто прибавлять по 1 к каждому аккаунту

    public void go(){
        for(int i=1; i<6; i++){
            fileProcesses.writeAccounts(".\\src\\main\\resources\\Accounts\\account"+ i +".ser", new Account(i, "Account" + i , random.nextInt(100000)));
        }

    }



}
