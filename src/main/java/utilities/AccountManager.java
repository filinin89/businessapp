package utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.Transaction;
import thread.Transfer;
import userData.Account;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class AccountManager {

    private Logger logger = LoggerFactory.getLogger(AccountManager.class);
    private Transaction transaction = new Transaction();
    private ArrayList<Account> accounts; // после загрузки из файла этот класс отвечаетза хранение аккаунтов


    public void loadAccounts(){
        FileProcesses fileProcesses = new FileProcesses();
        String path = ".\\src\\main\\resources\\Accounts\\";
        try {
            accounts = fileProcesses.readAccountsFromFolder(path);
        } catch (ClassNotFoundException e) {
            logger.error("ClassNotFound");
        }
    }

    private long calculateSum(){
        return accounts.stream().mapToLong(balance->balance.getBalance()).sum();
    }

    public void printBalanceAndSum(){

        for(Account account : accounts){
            logger.info("Account" + account.getId() + "  balance " + account.getBalance());
        }
        logger.info("Сумма всех счетов " + calculateSum());

    }




    public void startExchange(int threadCount, int maxTransaction){

        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);

        for(int i =0 ; i<threadCount; i++){
            executorService.execute(new Transfer(accounts, maxTransaction, transaction));
        }
        executorService.shutdown(); // закроем пул, разрешив доработать всем потокам в очереди

        while(!executorService.isTerminated()){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}
