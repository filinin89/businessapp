package utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import userData.Account;

import java.io.*;
import java.util.ArrayList;

public class FileProcesses {

    private ArrayList<Account> accountsList;
    private Logger logger = LoggerFactory.getLogger(FileProcesses.class);



    public void writeAccounts(String path, Account account){

        File file = new File(path);

        try(ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(file))){
            os.writeObject(account);

        }catch (IOException e){
            logger.info(e.getMessage());
        }
    }


    public ArrayList readAccountsFromFolder(String path) throws ClassNotFoundException {
        File folder = new File(path);
        accountsList = new ArrayList<>();

        for(File file : folder.listFiles()){ // можно использовать Files.walk(Paths.get())
            try (ObjectInputStream os = new ObjectInputStream(new FileInputStream(file))) {
                accountsList.add((Account)os.readObject()); // можно сделать проверку если файл и этот файд называется как нужно, тогда записывать

            } catch (IOException e)
            {
                logger.info(e.getMessage());
            }
        }
        return accountsList;

    }


}
