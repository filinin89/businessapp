package userData;

import java.io.Serializable;
import java.util.concurrent.locks.ReentrantLock;

public class Account implements Serializable { // объекты этого класса будут сериализовываться

    private int id;
    private String name;
    private long balance;


    public Account(int id, String name, long balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getBalance() {
        return balance;
    }
}
